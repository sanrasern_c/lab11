import java.io.IOException;
import java.util.Scanner;

import com.lloseng.ocsf.client.AbstractClient;
public class ChatClient extends AbstractClient {
	
	public ChatClient (String host, int port ) {
		super(host , port);
	}
	
	@Override
	protected void handleMessageFromServer(Object msg) {
		System.out.println("> "+ msg);
	}
	
	public static void main(String[] args) throws IOException {
		Scanner scan = new Scanner(System.in);
		String host = "158.108.135.197";
		int port = 5555;
		ChatClient cl = new ChatClient(host, port);
		
		cl.openConnection();
		while(cl.isConnected()) {
		String str = scan.nextLine();
		if(str.equals("quit")) cl.closeConnection();
		else cl.sendToServer(str);
	}
		System.out.println("Disconnected!");

	}
	
	
	
	
}
