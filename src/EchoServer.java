import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;


public class EchoServer extends AbstractServer{
	final static int NOTLOGGEDIN = 0;
	final static int LOGGEDIN = 1;
	private static final int PORT = 5555;
	private final String host = "ddd";

	private List<ConnectionToClient> clients = new ArrayList<ConnectionToClient>();
	/**To pm*/
	Map<String,String> map = new HashMap<String,String>();


	public EchoServer(int port) {
		super(port);
	}

	protected void clientConnected(ConnectionToClient client) {
		super.sendToAllClients("Connect to "+ host);
		client.setInfo("state", NOTLOGGEDIN);
		clients.add(client);
	}

	public void sendToClient(ConnectionToClient client , String str) {
		try {
			client.sendToClient(str);
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		if(!(msg instanceof String)) {
			sendToClient(client, "Unknown message");
		}

		//client.setInfo("state", NOTLOGGEDIN);
		String message = (String)msg;
		int state = (Integer)client.getInfo("state");
		String username = "";

		switch(state) {
		case NOTLOGGEDIN:
			if(message.matches("Login \\w+")) {
				username = message.substring(6).trim();
				client.setInfo("username", username);
				map.put(username, "");
				System.out.println(username + "logged in");
				super.sendToAllClients(username + " logged in"); 
				client.setInfo("state", LOGGEDIN);
			}
			else sendToClient(client, "Please log in");
			break;



		case LOGGEDIN:
			username = (String) client.getInfo("username");
			if(message.equalsIgnoreCase("Logout")) {
				super.sendToAllClients(username + " logged out");
				System.out.println(username + " logged out");
				map.remove(username);
				client.setInfo("state", NOTLOGGEDIN);
			}
			else if(message.matches("To: \\w+")) {
				if(map.containsKey(message.substring(3).trim())) 
					map.replace(username, message.substring(3).trim());
			}
			else if(message.length() > 0) {
				username = (String)client.getInfo("username");
				if(!map.get(username).equals("")){
					Thread[] thr = this.getClientConnections();
					ConnectionToClient[] c = new ConnectionToClient[thr.length];
					for(int i=0; i<c.length; i++) {
						c[i] = (ConnectionToClient)thr[i];
					}
//					ConnectionToClient[] c = (ConnectionToClient[])this.getClientConnections();
					for(int i=0; i< c.length; i++) {
						if(c[i].getInfo("username").equals(map.get(username))) {
							try {
								c[i].sendToClient(message);
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}
				else {	
					System.out.println(username + " : " + message);
					super.sendToAllClients(username + " : " + message);
				}
			}
			break;
		}
		
	}


	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		EchoServer server = new EchoServer (PORT);
		try{
			server.listen();
		
		}catch(IOException e) {

		}

	}


}